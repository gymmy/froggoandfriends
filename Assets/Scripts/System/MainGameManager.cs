﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// GameObjects that use Monobehavior's update should use this instead for faster update and ability to pause 
/// </summary>
public interface IUpdateable
{
    void ManagedUpdate();
}

/// <summary>
/// Responsibility: Handle main game flow.
/// </summary>
public class MainGameManager : MonoBehaviour
{
    public enum GameState
    {
        Standby,
        Start,
        Update,
        Pause,
    }

    private GameState m_currentGameState = GameState.Start;
    [SerializeField] private WorldAssembler m_worldAssembler;

    private List<IUpdateable> m_updateables = new List<IUpdateable>();

    void Start()
    {
        //Initialize PlayerController
        PlayerController.Get();

        // Assemble Game World
        m_worldAssembler.Initialize();
        m_worldAssembler.GenerateWorld("World11");

        // Init updateables
        if (m_updateables == null)
            m_updateables = new List<IUpdateable>();

        m_updateables = FindUpdateables();
        Debug.Log(m_updateables.Count);

        // Set default player actor
        SwapActorEvent _swapActorEvent = new SwapActorEvent(0, this);
        GameEventManager.BroadcastEvent<SwapActorEvent>(_swapActorEvent);

        // Pause game on dialogue trigger
        GameEventManager.AddListener<DialogueTriggeredEvent>(PauseGameEvent);
        GameEventManager.AddListener<DialogueEndedEvent>(ResumeGameEvent);

        // Pause game on dialogue trigger
        GameEventManager.AddListener<DeathEvent>(PlayerDeathEvent);
    }

    public void PlayerDeathEvent(GameEvent p_event)
    {
        DeathEvent _castedEvent = (DeathEvent)p_event;
        if (_castedEvent.m_DeadActor is FrogActor || _castedEvent.m_DeadActor is EagleActor)
        {
            RestartLevel();
        }
    }

    public void PauseGameEvent(GameEvent p_event)
    {
        Pause();
    }

    public void ResumeGameEvent(GameEvent p_event)
    {
        Resume();
    }

    public void Pause()
    {
        SetState(GameState.Pause);
    }

    public void Resume()
    {
        SetState(GameState.Update);
    }


    private void SetState(GameState p_state)
    {
        m_currentGameState = p_state;
    } 

    public void Update()
    {
        if (m_currentGameState != GameState.Update)
        {
            return;
        }

        for (int i = 0; i < m_updateables.Count; ++i)
        {
            m_updateables[i].ManagedUpdate();
        }
    }


    public List<IUpdateable> FindUpdateables()
    {
        List<IUpdateable> _interfaces = new List<IUpdateable>();
        GameObject[] _rootGameObjects = SceneManager.GetActiveScene().GetRootGameObjects();
        foreach (var _rootGameObject in _rootGameObjects)
        {
            IUpdateable[] _childrenInterfaces = _rootGameObject.GetComponentsInChildren<IUpdateable>();
            foreach (var _childInterface in _childrenInterfaces)
            {
                _interfaces.Add(_childInterface);
            }
        }
        return _interfaces;
    }

    public static void RestartLevel()
    {
        SceneManager.LoadScene(0);
    }
}