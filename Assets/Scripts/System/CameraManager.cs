﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Follows player unless they go past the specified limits.
/// </summary>
public class CameraManager : MonoBehaviour
{
    public Actor m_TargetActor;
    public float m_verticalOffset;
    public float m_lookAheadDistanceX;
    public float m_lookSmoothTimeX;
    public float m_verticalSmoothTime;
    public Vector2 m_focusAreaSize;

    private FocusArea m_focusArea;

    float m_currentLookAheadX;
    float m_targetLookAheadX;
    float m_lookAheadDirectionX;
    float m_smoothLookVelocityX;
    float m_smoothVelocityY;

    [SerializeField] private float m_yMinLimit;
    [SerializeField] private float m_yMaxLimit;
   
    private void OnEnable()
    {
        GameEventManager.AddListener<PossessActorEvent>(OnActorPossessed);
    }

    private void OnDisable()
    {
        GameEventManager.RemoveListener<PossessActorEvent>(OnActorPossessed);
    }

    private void OnActorPossessed(GameEvent p_gameEvent)
    {
        PossessActorEvent _gameEvent = (PossessActorEvent)p_gameEvent;
        SetTarget(_gameEvent.m_PossessedActor);
    }

    public void SetTarget(Actor p_targetActor)
    {
        m_TargetActor = p_targetActor;
        m_focusArea = new FocusArea(m_TargetActor.GetComponent<Collider2D>().bounds, m_focusAreaSize);
    }

    // Put camera update in LateUpdate to let player movement complete first
    private void LateUpdate()
    {
        if (!CanFollowActor())
        {
            return;
        }

        m_focusArea.Update(m_TargetActor.GetComponent<Collider2D>().bounds);

        Vector2 _focusPosition = m_focusArea.m_Center + Vector2.up * m_verticalOffset;

        if (m_focusArea.m_Velocity.x != 0)
        {
            m_lookAheadDirectionX = Mathf.Sign(m_focusArea.m_Velocity.x);
        }

        m_targetLookAheadX = m_lookAheadDirectionX * m_lookAheadDistanceX;
        m_currentLookAheadX = Mathf.SmoothDamp(m_currentLookAheadX, m_targetLookAheadX, ref m_smoothLookVelocityX, m_lookSmoothTimeX);

        _focusPosition += Vector2.right * m_currentLookAheadX;

        transform.position = (Vector3)_focusPosition + Vector3.forward * -10;
    }

    private bool CanFollowActor()
    {
        return transform.position.y > m_yMinLimit && transform.position.y < m_yMaxLimit;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = new Color(1, 0, 0, 0.5f);
        Gizmos.DrawCube(m_focusArea.m_Center, m_focusAreaSize);
    }

    struct FocusArea
    {
        public Vector2 m_Center;
        public Vector2 m_Velocity;
        float m_left, m_right;
        float m_top, m_bottom;

        public FocusArea(Bounds p_targetBounds, Vector2 p_size)
        {
            m_left = p_targetBounds.center.x - p_size.x / 2;
            m_right = p_targetBounds.center.x + p_size.x / 2;

            m_bottom = p_targetBounds.min.y;
            m_top = p_targetBounds.min.y + p_size.y;

            m_Velocity = Vector2.zero;
            m_Center = new Vector2((m_left + m_right)/2, (m_top+m_bottom)/2);
        }

        public void Update(Bounds p_targetBounds)
        {
            float _shiftX = 0;
            if (p_targetBounds.min.x < m_left)
            {
                _shiftX = p_targetBounds.min.x - m_left;
            }
            else if (p_targetBounds.max.x > m_right)
            {
                _shiftX = p_targetBounds.max.x - m_right;
            }

            m_left += _shiftX;
            m_right += _shiftX;

            float _shiftY = 0;
            if (p_targetBounds.min.y < m_bottom)
            {
                _shiftY = p_targetBounds.min.y - m_bottom;
            }
            else if (p_targetBounds.max.y > m_top)
            {
                _shiftY = p_targetBounds.max.y - m_top;
            }

            m_top += _shiftY;
            m_bottom += _shiftY;
            m_Center = new Vector2((m_left + m_right)/2 , (m_top + m_bottom)/2);
            m_Velocity = new Vector2(_shiftX, _shiftY);
        }
    }
}
