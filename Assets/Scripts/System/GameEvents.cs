﻿/// Container for all game event types/templates
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Base class for all game events.
/// </summary>
public abstract class GameEvent : UnityEngine.Object
{
    public GameEvent(UnityEngine.Object p_Source)
    {
        m_Source = p_Source;
    }

    UnityEngine.Object m_Source;
}

/// <summary>
/// Fired on Start() of MainGameManager
/// </summary>
public class GameStartEvent : GameEvent
{
    public GameStartEvent(UnityEngine.Object p_Source) : base(p_Source) { }
}

/// <summary>
/// Fired by PlayerController on key input
/// </summary>
public class AnyKeyPressedEvent : GameEvent
{
    public AnyKeyPressedEvent(UnityEngine.Object p_Source) : base(p_Source) { }
}

/// <summary>
/// Fired by PlayerController on actor/animal swapping
/// </summary>
public class SwapActorEvent : GameEvent
{
    public readonly int m_SwapActorIndex;
    public SwapActorEvent(int p_swapActorIndex, UnityEngine.Object p_Source) : base(p_Source)
    {
        m_SwapActorIndex = p_swapActorIndex;
    }
}

/// <summary>
/// Fired by PlayerController on possess actor
public class PossessActorEvent : GameEvent
{
    public readonly Actor m_PossessedActor;
    public PossessActorEvent(Actor p_possessedActor, UnityEngine.Object p_Source) : base(p_Source)
    {
        m_PossessedActor = p_possessedActor;
    }
}

/// <summary>
/// Fired by DamageHandler on Damageable death
public class DeathEvent : GameEvent
{
    public readonly Actor m_DeadActor;
    public DeathEvent(Actor p_deadActor, UnityEngine.Object p_Source) : base(p_Source)
    {
        m_DeadActor = p_deadActor;
    }
}

/// <summary>
/// Fired on any achievement accomplished
public class AchievementCompletedEvent : GameEvent
{
    public readonly string m_Name;
    public readonly string m_Description;
     
    public AchievementCompletedEvent(string p_name, string p_description, UnityEngine.Object p_Source) : base(p_Source)
    {
        m_Name = p_name;
        m_Description = p_description;
    }
}

/// Fired on any dialogue triggered
public class DialogueTriggeredEvent : GameEvent
{
    public DialogueTriggeredEvent(UnityEngine.Object p_Source) : base(p_Source) { }
}

/// Fired on any dialogue ended
public class DialogueEndedEvent : GameEvent
{
    public DialogueEndedEvent(UnityEngine.Object p_Source) : base(p_Source) { }
}