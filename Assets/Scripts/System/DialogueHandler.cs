﻿/// <summary>
/// DialogueHandler: Translate dialogue JSON data into UI text.
/// Works by rebuilding binary tree first and then assigning the dialogue to each node afterwards.
/// </summary>

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;
using System.Text.RegularExpressions;

[System.Serializable]
public class SerializedDialogueData
{
    public int m_ID;
    public Vector3 m_GraphPosition;
    public string m_Writer;
    public string m_Message;
}

[System.Serializable]
public class SerializedJsonPack
{
    public string m_TreeData;
    public SerializedDialogueData[] m_DialogueData;
}

[System.Serializable]
public class DialogueNode
{
    public DialogueNode m_LeftNode;
    public DialogueNode m_RightNode;

    public SerializedDialogueData BaseData = new SerializedDialogueData();
}

public class DialogueHandler : MonoBehaviour
{
    [Header("Dialogue")]
    [SerializeField] private GameObject m_dialogueRoot;

    [Header("Choice")]
    [SerializeField] private GameObject m_choiceRoot;
    [SerializeField] private Text m_choiceAText;
    [SerializeField] private Text m_choiceBText;
    [SerializeField] private DialogueWidget m_widget;

    private DialogueNode m_curNode;
    private List<DialogueNode> m_nodes = new List<DialogueNode> (); 
    private string m_saveDirectory;

    private void Start()
    {
        LoadEditorData("Intro");
        RunDialogue();
    }

    private string GetSaveDirectory()
    {
        if (m_saveDirectory == null)
        {
            m_saveDirectory = "Assets/Dialogues/";
        }

        return m_saveDirectory;
    }

    public DialogueNode DeserializeNode(string p_data)
    {
        var localData = p_data;
        return DeserializeNode(ref localData);
    }

    public DialogueNode DeserializeNode(ref string p_data)
    {
        DialogueNode _root = null;

        // If reached the end of the string data
        if (p_data.IndexOf(",", 1) == -1)
        {
            return _root;
        }

        // Calculate Value
        var _val = p_data.Substring(",".Length, p_data.Substring(",".Length).IndexOf(","));

        // If no value present
        if (string.IsNullOrEmpty(_val))
        {
            return _root;
        }

        string _id = Regex.Match(_val, @"-?\d+").Value;
        string _nodeType = _val.Replace(_id, "");

        // Evaluate Root
        if (_nodeType == "Dialogue")
        {
            _root = new DialogueNode();
            m_nodes.Add(_root);
        }
        else
        {
            Debug.Log("S6Error: Cannot deserialize symbol: " + _val);
        }

        _root.BaseData.m_ID = int.Parse(_id);

        // Evaluate Left
        if (_root != null)
        {
            p_data = p_data.Substring(p_data.IndexOf(",", ",".Length));
            _root.m_LeftNode = DeserializeNode(ref p_data);

            // Evaluate Right
            p_data = p_data.Substring(p_data.IndexOf(",", ",".Length));
            _root.m_RightNode = DeserializeNode(ref p_data);
        }

        return _root;
    }

    private void LoadEditorData(string p_serializeFileName)
    {
        if (m_nodes != null)
        {
            m_nodes.Clear();
        }

        string _file = System.IO.Path.Combine(GetSaveDirectory(), p_serializeFileName + ".txt");
        if (_file == null || _file == "")
        {
            return;
        }

        StreamReader _streamReader = new StreamReader(_file);
        string _json = _streamReader.ReadToEnd();
        _streamReader.Close();

        SerializedJsonPack _serializedNodeData = JsonUtility.FromJson<SerializedJsonPack>(_json);

        var _test = DeserializeNode(_serializedNodeData.m_TreeData);

        for (int i = 0; i < _serializedNodeData.m_DialogueData.Length; ++i)
        {
            SerializedDialogueData _itrData = _serializedNodeData.m_DialogueData[i];
            DialogueNode _match = SearchNode(_itrData.m_ID) as DialogueNode;

            if (_match == null)
            {
                Debug.Log("S6Error: No matching node found for ID: " + _itrData.m_ID);
                return;
            }
            _match.BaseData.m_Writer = _itrData.m_Writer;
            _match.BaseData.m_Message = _itrData.m_Message;
        }
    }

    private DialogueNode SearchNode(int _nodeID)
    {
        for (int i = 0; i < m_nodes.Count; ++i)
        {
            if (m_nodes[i].BaseData.m_ID == _nodeID)
            {
                return m_nodes[i];
            }
        }
        return null;
    }

    public void RunDialogue()
    {
        m_dialogueRoot.gameObject.SetActive(true);
        SetNextNode(m_nodes[0]);
        RunNextNode();

        DialogueTriggeredEvent _dialogueEvent = new DialogueTriggeredEvent(this);
        GameEventManager.BroadcastEvent<DialogueTriggeredEvent>(_dialogueEvent);
    }

    private void SetNextNode(DialogueNode node)
    {
        m_curNode = node;
    }

    private void DisplayDialogue(DialogueNode node)
    {
        if (!(node.BaseData is SerializedDialogueData))
        {
            return;
        }

        SerializedDialogueData data = m_curNode.BaseData as SerializedDialogueData;
        m_widget.Writer.text = data.m_Writer;
        m_widget.Message.text = data.m_Message;
    }

    public void EndDialogue()
    {
        m_dialogueRoot.gameObject.SetActive(false);

        DialogueEndedEvent _dialogueEndedEvent = new DialogueEndedEvent(this);
        GameEventManager.BroadcastEvent<DialogueEndedEvent>(_dialogueEndedEvent);
    }

    private void RunNextNode()
    {
        if (m_curNode == null)
        {
            EndDialogue();
            return;
        }

        // Read next node as dialogue
        if (m_curNode.BaseData is SerializedDialogueData)
        {
            SerializedDialogueData data = m_curNode.BaseData as SerializedDialogueData;
            DisplayDialogue(m_curNode);

            // means curnode is a dialogue
            if (m_curNode.m_LeftNode != null && m_curNode.m_RightNode == null)
            {
                m_curNode = m_curNode.m_LeftNode; // (NOTE) Left node is default next node if right node is empty
            }
            // means curnode is a choice
            else if (m_curNode.m_LeftNode != null && m_curNode.m_RightNode != null)
            {
                m_choiceRoot.SetActive(true);
                SerializedDialogueData lData = m_curNode.m_LeftNode.BaseData as SerializedDialogueData;
                m_choiceAText.text = lData.m_Message;

                SerializedDialogueData rData = m_curNode.m_RightNode.BaseData as SerializedDialogueData;
                m_choiceBText.text = rData.m_Message;
            }
            else if (m_curNode.m_LeftNode == null)
            {
                m_curNode = null;
                Debug.Log("SideQuest: No more left node. Showing close button");
            }
        }
    }

    public void Update()
    {
        if (gameObject.activeInHierarchy)
        {
            if (Input.GetMouseButtonDown(0))
            {
                RunNextNode();
            }
        }
    }

    public void OnPressedButtonA()
    {
        m_choiceRoot.SetActive(false);
        if (m_curNode != null && m_curNode.m_LeftNode != null && m_curNode.m_LeftNode.m_LeftNode != null)
        {
            SetNextNode(m_curNode.m_LeftNode.m_LeftNode);
            RunNextNode();
        }
        else
        {
            Debug.LogErrorFormat("{0} | {1}", m_curNode, m_curNode.m_LeftNode);
        }
    }
    public void OnPressedButtonB()
    {
        m_choiceRoot.SetActive(false);
        if (m_curNode != null && m_curNode.m_RightNode != null && m_curNode.m_RightNode.m_LeftNode != null)
        {
            SetNextNode(m_curNode.m_RightNode.m_LeftNode);
            RunNextNode();
        }
        else
        {
            Debug.LogErrorFormat("{0} | {1}", m_curNode, m_curNode.m_RightNode);
        }
    }
}
