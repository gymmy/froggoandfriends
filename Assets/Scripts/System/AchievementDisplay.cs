﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// There is no save system implemented yet so Achievement system is simply for display.
/// </summary>
/// 
public abstract class Achievement : UnityEngine.Object
{
    public readonly string m_Name = "Unknown Achievement";
    public readonly string m_Description = "Unknown Description";

    public Achievement(string p_name, string p_description)
    {
        m_Name = p_name;
        m_Description = p_description;
    }

    public void Start()
    {
        BindEvents();
    }

    public abstract void BindEvents();
    public abstract void UnbindEvents();
    public virtual void HandleGameEvent(GameEvent p_gameEvent) { }

    public void Complete()
    {
        AchievementCompletedEvent _achievementCompletedEvent = new AchievementCompletedEvent(m_Name, m_Description, this);
        GameEventManager.BroadcastEvent<AchievementCompletedEvent>(_achievementCompletedEvent);
        UnbindEvents();
    }
}

public class InputAchievement : Achievement
{
    public InputAchievement(string p_name, string p_description) : base(p_name, p_description) { }

    public override void BindEvents()
    {
        GameEventManager.AddListener<AnyKeyPressedEvent>(HandleGameEvent);
    }

    public override void UnbindEvents()
    {
        GameEventManager.RemoveListener<AnyKeyPressedEvent>(HandleGameEvent);
    }

    public override void HandleGameEvent(GameEvent p_gameEvent)
    {
        if (p_gameEvent is AnyKeyPressedEvent)
        {
            Complete();
        }
    }
}

public class KillEnemyAchievement : Achievement
{
    public KillEnemyAchievement(string p_name, string p_description) : base(p_name, p_description) { }

    public override void BindEvents()
    {
        GameEventManager.AddListener<DeathEvent>(HandleGameEvent);
    }

    public override void UnbindEvents()
    {
        GameEventManager.RemoveListener<DeathEvent>(HandleGameEvent);
    }

    public override void HandleGameEvent(GameEvent p_gameEvent)
    {
        DeathEvent _castedEvent = (DeathEvent) p_gameEvent;
        if (_castedEvent.m_DeadActor is RatEnemyActor)
        {
            Complete();
        }
    }
}

public class AchievementDisplay : MonoBehaviour
{
    [SerializeField] private AchievementWidget m_widget;

    public void Start()
    {
        InputAchievement _inputAchievement = new InputAchievement("Detecting Input!", "One requirement down.");
        _inputAchievement.Start();

        KillEnemyAchievement _killAchievement = new KillEnemyAchievement("Another one bites the dust", "The enemy killed and the achievement requirement.");
        _killAchievement.Start();

        // Bind UI display on achievement completion
        GameEventManager.AddListener<AchievementCompletedEvent>(CompleteAchievement);
    }

    public void OnDisable()
    {
        GameEventManager.RemoveListener<AchievementCompletedEvent>(CompleteAchievement);
    }

    public void CompleteAchievement(GameEvent p_gameEvent)
    {
        AchievementCompletedEvent _castedEvent = (AchievementCompletedEvent)p_gameEvent;
        m_widget.Display(_castedEvent.m_Name, _castedEvent.m_Description);
    }
}
