﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// Responsibility: Handle the storing of events and callbacks
public sealed class GameEventManager : ScriptableObject
{
    private static Dictionary<System.Type, List<System.Action<GameEvent>>> m_GameEventDict = new Dictionary<System.Type, List<System.Action<GameEvent>>>();

    /// <summary>
    /// Fire all callbacks listening to specified event.
    /// </summary>
    public static void BroadcastEvent<T>(GameEvent p_event) where T : GameEvent
    {
        List<System.Action<GameEvent>> _callbacks;
        m_GameEventDict.TryGetValue(typeof(T), out _callbacks);
        if (_callbacks != null)
        {
            for (int i = 0; i < _callbacks.Count; ++i)
            {
                _callbacks[i].Invoke(p_event);
            }
        }
    }

    /// <summary>
    /// Add callback to be fired when event is broadcasted.
    /// </summary>
    public static void AddListener<T>(System.Action<GameEvent> p_Callback) where T : GameEvent
    {
        if (m_GameEventDict == null)
        {
            return;
        }

        System.Type _type = typeof(T);

        if (m_GameEventDict.ContainsKey(_type))
        {
            m_GameEventDict[_type].Add(p_Callback);
        }
        else
        {
            m_GameEventDict.Add(_type, new List<System.Action<GameEvent>>() { p_Callback });
        }
    }

    /// <summary>
    /// Remove callback to subscribed to specified event.
    /// </summary>
    public static void RemoveListener<T>(System.Action<GameEvent> p_Callback) where T : GameEvent
    {
        if (m_GameEventDict == null)
        {
            return;
        }

        System.Type _type = typeof(T);

        if (m_GameEventDict.ContainsKey(_type))
        {
            m_GameEventDict[_type].Remove(p_Callback);

            // Delete game event key if there are no more listeners
            if (m_GameEventDict[_type].Count == 0)
            {
                m_GameEventDict.Remove(_type);
            }
        }
    }
}
