﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Text;

/// <summary>
/// This system uses a text file to generate a world by translating char keys as world tiles.
/// </summary>
public class WorldAssembler : MonoBehaviour
{
    class TileData
    {
        private int m_nextIndex = 0;
        private readonly List<Sprite> m_SpriteList = new List<Sprite>();

        public TileData(Sprite[] p_sprites)
        {
            if (m_SpriteList == null)
            {
                m_SpriteList = new List<Sprite>();
            }

            for (int i = 0; i < p_sprites.Length; ++i)
            {
                m_SpriteList.Add(p_sprites[i]);
            }
        }

        public Sprite GetSprite()
        {
            Sprite _sprite = m_SpriteList[m_nextIndex];

            if (m_nextIndex < m_SpriteList.Count-1)
            {
                ++m_nextIndex;
            }
            else
            {
                m_nextIndex = 0;
            }

            return _sprite;
        }
    }

    [Header("Actors")]
    [SerializeField] private GameObject m_playerPrefab;
    [SerializeField] private GameObject m_possumPrefab;
    [SerializeField] private GameObject m_eaglePrefab;

    [Header("World")]
    [SerializeField] private Sprite[] m_grassTiles;
    [SerializeField] private Sprite m_bushSprite;
    [SerializeField] private Sprite m_deepSoilTile;
    [SerializeField] private Sprite m_treeSprite;

    private TileData m_tileDataGrass;

    [SerializeField] private float m_tileWidth = 1.6f;
    [SerializeField] private float m_tileSize = 10f;

    // player
    private const char KEY_PLAYERSPAWN = 'S';

    // world
    private const char KEY_GRASS = 'G';
    private const char KEY_BUSH = 'B';
    private const char KEY_DEEPSOIL = 'X';
    private const char KEY_TREE = 'T';
    private const char KEY_SPACE = '-';

    // enemies
    private const char KEY_POSSUM = 'P';
    private const char KEY_EAGLE = 'E';

    public void Initialize()
    {
        if (m_tileDataGrass == null)
        {
            m_tileDataGrass = new TileData(m_grassTiles);
        }
    }

    public void GenerateWorld(string p_worldName)
    {
        StringBuilder _stringBuilder = new StringBuilder("Assets/Scripts/World/");
        _stringBuilder.Append(p_worldName);
        _stringBuilder.Append(".txt");
        string path = _stringBuilder.ToString();

        StreamReader reader = new StreamReader(path);

        int _y = 0;
        string _line;
        while ((_line = reader.ReadLine()) != null)
        {
            for (int x = 0; x < _line.Length; ++x)
            {
                SpawnWorldTile(_line[x], x * m_tileWidth, -(_y * m_tileWidth));
            }

            _y++;
        }

        reader.Close();
    }

    private void SpawnWorldTile(char p_tileData, float x, float y)
    {
        // Build tile name
        StringBuilder _stringBuilder = null;
        System.Action BuildTileName = () =>
        {
            _stringBuilder = new StringBuilder("Tile");
            _stringBuilder.
                Append("[").
                Append(x).
                Append(",").
                Append(y).
                Append("]");
        };

        // Build tile gameobject
        GameObject _tile = null;
        SpriteRenderer _spriteRenderer = null;

        System.Action BuildTileGameObject = () =>
        {
            _tile = new GameObject(_stringBuilder.ToString());
            _tile.transform.SetParent(transform);
            _tile.transform.localPosition = new Vector3(x, y);
            _tile.transform.localScale = new Vector2(m_tileSize, m_tileSize);
            _spriteRenderer = _tile.AddComponent<SpriteRenderer>();
        };

        switch (p_tileData)
        {
            case KEY_GRASS:
                BuildTileName();
                BuildTileGameObject();

                if (_spriteRenderer)
                {
                    _spriteRenderer.sprite = m_tileDataGrass.GetSprite();
                    _tile.AddComponent<BoxCollider2D>();
                }

                break;

            case KEY_BUSH:
                BuildTileName();
                BuildTileGameObject();

                if (_spriteRenderer)
                {
                    _spriteRenderer.sprite = m_bushSprite;
                }
                break;

            case KEY_TREE:
                BuildTileName();
                BuildTileGameObject();

                _tile.transform.localScale = new Vector3(10,10,10);

                if (_spriteRenderer)
                {
                    _spriteRenderer.sprite = m_treeSprite;
                }
                break;

            case KEY_DEEPSOIL:
                BuildTileName();
                BuildTileGameObject();

                if (_spriteRenderer)
                {
                    _spriteRenderer.sprite = m_deepSoilTile;
                }
                break;

            case KEY_PLAYERSPAWN:
                GameObject _player = Instantiate(m_playerPrefab);
                _player.transform.SetParent(transform);
                _player.transform.localPosition = new Vector3(x, y);
                break;

            case KEY_POSSUM:
                GameObject _possum = Instantiate(m_possumPrefab);
                _possum.transform.SetParent(transform);
                _possum.transform.localPosition = new Vector3(x, y);
                break;

            case KEY_EAGLE:
                GameObject _eagle = Instantiate(m_eaglePrefab);
                _eagle.transform.SetParent(transform);
                _eagle.transform.localPosition = new Vector3(x, y);
                break;

            default:
                break;
        }
    }
}
