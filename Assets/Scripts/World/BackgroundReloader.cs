﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;

public class BackgroundReloader : MonoBehaviour
{
    [SerializeField] private Sprite m_backgroundSprite;
    [SerializeField] private int m_instanceCount;
    [SerializeField] private float m_instanceSize;

    private List<SpriteRenderer> m_backgroundInstances = new List<SpriteRenderer>();

    private void OnEnable()
    {
       // Initialize();
    }

    private void Initialize()
    {
        for (int i = 0; i < m_instanceCount; ++i)
        {
            StringBuilder _stringBuilder = null;
            _stringBuilder = new StringBuilder("BG");
            _stringBuilder.
                Append("[").
                Append(i).
                Append("]");

            GameObject _instance = new GameObject(_stringBuilder.ToString());

            SpriteRenderer _spriteRenderer = _instance.AddComponent<SpriteRenderer>();
            _spriteRenderer.sprite = m_backgroundSprite;
            _instance.transform.SetParent(transform);
            _instance.transform.localScale = Vector3.one * m_instanceSize;
            _instance.transform.localPosition = new Vector2(1.5f*(i * m_instanceSize), 0);

            m_backgroundInstances.Add(_spriteRenderer);
        }

    
    }

}
