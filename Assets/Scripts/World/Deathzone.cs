﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Deathzone : MonoBehaviour
{
    public void OnTriggerEnter2D(Collider2D collision)
    {
        IDamageable _damageable = collision.gameObject.GetComponent<IDamageable>();
        if (_damageable != null)
        {
            if (_damageable.CanTakeDamage(null))
            {
                _damageable.TakeDamage(9999999, null);
            }
        }
    }
}
