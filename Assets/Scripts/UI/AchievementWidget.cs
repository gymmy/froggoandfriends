﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AchievementWidget : MonoBehaviour
{
    public Text m_Title;
    public Text m_Description;
    [SerializeField] private float m_expireTime;

    public void Display(string p_title, string p_description)
    {
        m_Title.text = p_title;
        m_Description.text = p_description;

        gameObject.SetActive(true);
        StartCoroutine(Expire());
    }

    IEnumerator Expire()
    {
        yield return new WaitForSeconds(m_expireTime);
        gameObject.SetActive(false);
    }
}
