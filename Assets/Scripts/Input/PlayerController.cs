﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Responsibility: Input handling for player.
/// Demonstrates use of delegates and event systems
/// </summary>
public class PlayerController : MonoBehaviour, IUpdateable
{
    private static PlayerController m_Instance;
    public static PlayerController Get()
    {
        if (!m_Instance)
        {
            GameObject _GameObject = new GameObject("PlayerController");
            m_Instance = _GameObject.AddComponent<PlayerController>();
        }

        return m_Instance;
    }

    private Actor m_currentActor;

    System.Action<float> OnMoveHorizontal;
    System.Action<float> OnMoveVertical;
    System.Action OnFireDown;
    System.Action OnFireUp;

    public Vector3 GetPlayerPosition()
    {
        return m_currentActor != null ? m_currentActor.transform.position : Vector3.zero;
    }

    public void Possess(Actor p_Actor)
    {
        if (p_Actor == null)
        {
            return;
        }

        m_currentActor = p_Actor;

        OnMoveVertical = (Direction) => m_currentActor.MoveVertical(Direction);
        OnMoveHorizontal = (Direction) => m_currentActor.MoveHorizontal(Direction);
        OnFireDown = () => m_currentActor.FireDown();
        OnFireUp = () => m_currentActor.FireUp();

        // Notify other systems of possession
        PossessActorEvent _possessActorEvent = new PossessActorEvent(m_currentActor, this);
        GameEventManager.BroadcastEvent<PossessActorEvent>(_possessActorEvent);
    }

    public void ClearPossess()
    {
        m_currentActor = null;
        OnMoveVertical = (Direction) => { };
        OnMoveHorizontal = (Direction) => { };
        OnFireDown = () => { };
        OnFireUp = () => { };
    }

    public void ManagedUpdate()
    {
        UpdateControls();
    }

    private void UpdateControls()
    {
        // For input achievement requirement
        if (Input.anyKeyDown)
        {
            AnyKeyPressedEvent _keyPressEvent = new AnyKeyPressedEvent(this);
            GameEventManager.BroadcastEvent<AnyKeyPressedEvent>(_keyPressEvent);
        }

        if (Input.GetButtonDown("Fire"))
        {
            OnFireDown.Invoke();
        }

        if (Input.GetButtonUp("Fire"))
        {
            OnFireUp.Invoke();
        }

        OnMoveHorizontal(Input.GetAxis("Horizontal"));
        OnMoveVertical(Input.GetAxis("Vertical"));

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            SwapActorEvent _swapActorEvent = new SwapActorEvent(0, this);
            GameEventManager.BroadcastEvent<SwapActorEvent>(_swapActorEvent);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            SwapActorEvent _swapActorEvent = new SwapActorEvent(1, this);
            GameEventManager.BroadcastEvent<SwapActorEvent>(_swapActorEvent);
        }
        if (Input.GetKeyDown(KeyCode.Equals))
        {
            MainGameManager.RestartLevel();
        }
    }
}
