﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using System.IO;
using System;
using System.Text.RegularExpressions;

/// <summary>
/// Editor UI tool for easy visual dialogue construction
/// Translates nodes to binary tree to be reassembled by DialogueHandler on runtime
/// </summary>

#if UNITY_EDITOR
using UnityEditor;

public sealed class VisualEditor : EditorWindow {
	private List<EditorNode>			m_nodes  = new List<EditorNode>();
	private List<EditorConnection>	    m_connections  = new List<EditorConnection>();

	private GUIStyle					m_nodeStyle;
	private GUIStyle					m_selectedNodeStyle;

	private GUIStyle					m_inPointStyle;
	private GUIStyle					m_outPointStyle;

	private EditorConnectionPoint		m_selectedInPoint;
	private EditorConnectionPoint		m_selectedOutPoint;

	private Vector2						m_drag;

	private float						m_menuBarHeight = 20.0f;
	private Rect						m_menuBar;

	#region Unity GUI Methods
	[MenuItem("Window/Dialogue Visual Editor")]
	private static void OpenWindow() {
		VisualEditor window = GetWindow<VisualEditor>();
		window.titleContent = new GUIContent("Dialogue Visual Editor");
	}

	private void OnEnable() {
		m_inPointStyle = new GUIStyle();
		m_inPointStyle.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/btn left.png") as Texture2D;
		m_inPointStyle.active.background = EditorGUIUtility.Load("builtin skins/darkskin/images/btn left on.png") as Texture2D;
		m_inPointStyle.border = new RectOffset(4, 4, 12, 12);

		m_outPointStyle = new GUIStyle();
		m_outPointStyle.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/btn right.png") as Texture2D;
		m_outPointStyle.active.background = EditorGUIUtility.Load("builtin skins/darkskin/images/btn right on.png") as Texture2D;
		m_outPointStyle.border = new RectOffset(4, 4, 12, 12);

		LoadEditorData();
	}
	#endregion

    Vector2 scrollPosition = Vector2.zero;

	#region GUI Draw
	void OnGUI() {
        Rect _rect;
        if(m_nodes.Count == 0) {
            _rect = new Rect(0, 0, position.width, position.height);
        } else {
            float xMin = float.MaxValue, xMax = float.MinValue;
            float yMin = float.MaxValue, yMax = float.MinValue;
            foreach(EditorNode node in m_nodes) {
                xMin = Mathf.Min(xMin, node.m_Rect.xMin);
                xMax = Mathf.Max(xMax, node.m_Rect.xMax + node.m_WidthOffset);
                yMin = Mathf.Min(yMin, node.m_Rect.yMin);
                yMax = Mathf.Max(yMax, node.m_Rect.yMax + node.m_HeightOffset);
            }
            _rect = new Rect(0, 0, xMax - xMin, yMax - yMin);
        }
            
		DrawGrid(5, 0.2f, Color.gray);
		DrawGrid(25, 0.6f, Color.gray);
        scrollPosition = GUI.BeginScrollView(new Rect(0, 0, position.width, position.height), scrollPosition, _rect);

		DrawNodes();
		DrawConnections();
		DrawConnectionLine(Event.current);

        GUI.EndScrollView();
		DrawMenuBar();
		ProcessNodeEvents(Event.current);
		ProcessEvents(Event.current);

		if (GUI.changed) {
			Repaint();
		}
	}

	private void DrawConnectionLine(Event p_event) {
		if (m_selectedInPoint == null && m_selectedOutPoint == null)
			return;

		Vector3 _origin = (m_selectedInPoint != null && m_selectedOutPoint == null) ? m_selectedInPoint.m_Rect.center : m_selectedOutPoint.m_Rect.center;
		Handles.DrawLine(_origin, p_event.mousePosition);
		GUI.changed = true;
	}
			 
	private void DrawConnections() {
		if (m_connections != null) {
			for(int i = 0; i < m_connections.Count; ++i) {
				m_connections[i].Draw();
			}
		}
	}

	private void DrawNodes() {
		if (m_nodes != null) {
			for (int i = 0; i < m_nodes.Count; ++i) {
				m_nodes[i].Draw();
			}
		}
	}

	private void DrawMenuBar() {
		m_menuBar = new Rect(0, 0, position.width, m_menuBarHeight);

		GUILayout.BeginArea(m_menuBar, EditorStyles.toolbar);
		GUILayout.BeginHorizontal();
            
        m_serializeDataFileName = GUILayout.TextArea(m_serializeDataFileName, EditorStyles.toolbarTextField, GUILayout.Width(65));

		if(GUILayout.Button(new GUIContent("Save"), EditorStyles.toolbarButton, GUILayout.Width(35))) {
			SaveEditorData();
		}

		GUILayout.Space(5);

		if(GUILayout.Button(new GUIContent("Load"), EditorStyles.toolbarButton, GUILayout.Width(35))) {
			LoadEditorData();
		}

		GUILayout.EndHorizontal();
		GUILayout.EndArea();
	}

	private void DrawGrid(float spacing, float opacity, Color color) {
		int _widthDivs = Mathf.CeilToInt(position.width / spacing);
		int _heightDivs = Mathf.CeilToInt(position.height / spacing);

		Handles.BeginGUI();
		Handles.color = new Color(color.r, color.g, color.b, opacity);

		for (int i = 0; i < _widthDivs; i++) {
			Handles.DrawLine(new Vector3(spacing * i, -spacing, 0), new Vector3(spacing * i, position.height, 0f));
		}

		for (int j = 0; j < _heightDivs; j++) {
			Handles.DrawLine(new Vector3(-spacing, spacing * j, 0), new Vector3(position.width, spacing * j, 0f));
		}

		Handles.color = Color.white;
		Handles.EndGUI();
	}
	#endregion

	#region GUI Events
	private void ProcessEvents(Event p_event) {
		m_drag = Vector2.zero;

		switch (p_event.type) {
			case EventType.MouseDown:
				if (p_event.button == 1) {
					ProcessContextMenu(p_event.mousePosition + scrollPosition);
				}
				break;

			case EventType.MouseDrag:
				if(p_event.button == 0) {
					OnDrag(p_event.delta);
				}
				break;
		}
	}

	private void ProcessNodeEvents(Event p_event) {
		if (m_nodes != null) {
			for (int i = m_nodes.Count - 1; i >= 0; --i) {
				bool guiChanged = m_nodes[i].ProcessEvents(p_event, scrollPosition);

				if (guiChanged) {
					GUI.changed = true;
				}
			}
		}
	}

	private void ProcessContextMenu(Vector2 p_mousePosition) {
		GenericMenu _genericMenu = new GenericMenu();

		_genericMenu.AddItem(new GUIContent("Add Dialogue"), false, () => CreateDialogueNode("Dialogue Node", p_mousePosition));

		_genericMenu.AddItem(new GUIContent("Clear Nodes"), false, () => {
			m_nodes.Clear();
			m_connections.Clear();
			ClearConnectionSelection();
		});

		_genericMenu.ShowAsContext();
	}
	#endregion

	#region Node
	private EditorNode FindNode(int p_id) {
		if (m_nodes == null) {
			return null;
		}

		for(int i = 0; i < m_nodes.Count; ++i) {
			if(m_nodes[i].m_ID == p_id) {
				return m_nodes[i];
			}
		}

		return null;
	}

	private EditorDialogueNode CreateDialogueNode(string p_name, Vector2 p_position) {
		if (m_nodes == null) {
			m_nodes = new List<EditorNode>();
		}

		EditorDialogueNode _newNode = new EditorDialogueNode(p_position, 200, 50, m_inPointStyle,
									m_outPointStyle, OnClickInPoint, OnClickOutPoint, OnClickRemoveNode, p_position.GetHashCode());

		_newNode.m_Title = "Dialogue Node";
		m_nodes.Add(_newNode);

        return _newNode;
	}

	private void OnClickInPoint(EditorConnectionPoint p_inPoint) {
		m_selectedInPoint = p_inPoint;

		if (m_selectedOutPoint != null) {
			if (m_selectedOutPoint.m_Node != m_selectedInPoint.m_Node) {
				CreateConnection();
				ClearConnectionSelection();
			}
			else {
				ClearConnectionSelection();
			}
		}
		else {
			ClearConnectionSelection();	
		}
	}

	private void OnClickOutPoint(EditorConnectionPoint p_outPoint) {
		m_selectedOutPoint = p_outPoint;

		if (m_selectedInPoint != null) {
			if (m_selectedOutPoint.m_Node != m_selectedInPoint.m_Node) {
				CreateConnection();
				ClearConnectionSelection();
			} else {
				ClearConnectionSelection();
			}
		}
	}

	private void OnClickRemoveConnection(EditorConnection p_connection) {
		EditorNode _node = p_connection.m_InPoint.m_Node;
		_node.m_Parent.RemoveChild(_node);
		m_connections.Remove(p_connection);
	}

	private void OnClickRemoveNode(EditorNode p_node) {
		if (m_connections != null) {
			List<EditorConnection> connectionsToRemove = new List<EditorConnection>();

			for (int i = 0; i < m_connections.Count; i++) {
				if (m_connections[i].m_InPoint == p_node.m_InPoint || m_connections[i].m_OutPoint == p_node.m_OutPoint) {
					connectionsToRemove.Add(m_connections[i]);
				}
			}

			for (int i = 0; i < connectionsToRemove.Count; i++) {
				m_connections.Remove(connectionsToRemove[i]);
			}

			connectionsToRemove = null;
		}

		m_nodes.Remove(p_node); 
	}

	private void CreateConnection() {
		if (m_connections == null) {
			m_connections = new List<EditorConnection>();
		}

		bool success = m_selectedOutPoint.m_Node.AddChild(m_selectedInPoint.m_Node);
		if(success) {
			m_connections.Add(new EditorConnection(m_selectedInPoint, m_selectedOutPoint, OnClickRemoveConnection));
		}
		else {
			Debug.Log("S6Error: Cannot connect node.");
		}
	}

	private void ClearConnectionSelection() {
		m_selectedInPoint = null;
		m_selectedOutPoint = null;
	}

	private void OnDrag(Vector2 delta) {
		m_drag = delta;
		GUI.changed = true;
	}
#endregion

	#region Data Serialization
	private string			m_saveDirectory;
	private string	        m_serializeDataFileName = "";

	private string GetSaveDirectory() {
		if (m_saveDirectory == null) {
            m_saveDirectory = "Assets/Dialogues/";
        }

		return m_saveDirectory;
	}

	private bool DoesSaveDataExist() {
		string file = System.IO.Path.Combine(GetSaveDirectory(), m_serializeDataFileName + ".dtsm");
		string fileInfoPath = System.IO.Path.Combine(GetSaveDirectory(), file);

		FileInfo fileCheck = new FileInfo(fileInfoPath);

		return fileCheck.Exists;
	}	

	private string SerializeNodeTree(EditorNode p_root, ref List<SerializedDialogueData> p_dataContainer) {
		string retVal = ",";
        if(p_root == null){
            return retVal;
        }

        SerializedDialogueData _nodeData = null;

        if(p_root is EditorDialogueNode) {
            SerializedDialogueData _dialogueData = new SerializedDialogueData();
              
			EditorDialogueNode _castedData = (EditorDialogueNode) p_root;
			_dialogueData.m_Message = _castedData.m_DialogueText;
			_dialogueData.m_Writer = _castedData.m_Speaker;

            _nodeData = _dialogueData;
		}

		_nodeData.m_ID = p_root.m_ID;
        _nodeData.m_GraphPosition = p_root.m_Rect.position;

        p_dataContainer.Add(_nodeData);

        retVal += p_root.m_ID + p_root.m_Title.Replace(" Node", "");
        retVal += SerializeNodeTree(p_root.m_LeftNode, ref p_dataContainer);
        retVal += SerializeNodeTree(p_root.m_RightNode, ref p_dataContainer);
        return retVal;
	}

	private void SaveEditorData() {
		SerializedJsonPack _sData = new SerializedJsonPack();
		_sData.m_DialogueData = new SerializedDialogueData[m_nodes.Count];
		string _json = "";
			
		List<SerializedDialogueData> genericDataContainer = new List<SerializedDialogueData>();
		_sData.m_TreeData = SerializeNodeTree(m_nodes[0], ref genericDataContainer);

        List<SerializedDialogueData> dialogueContainer = new List<SerializedDialogueData>();

        for (int i = 0; i < genericDataContainer.Count; ++i) {
            if (genericDataContainer[i] is SerializedDialogueData) {
                dialogueContainer.Add((SerializedDialogueData) genericDataContainer[i]);
            }
        }

        _sData.m_DialogueData = dialogueContainer.ToArray();

        _json = JsonUtility.ToJson(_sData, true);
		File.WriteAllText(System.IO.Path.Combine(GetSaveDirectory(), m_serializeDataFileName + ".txt"), _json);

		Debug.Log("Saved! Receipt: " + System.IO.Path.Combine(GetSaveDirectory(), m_serializeDataFileName));
	}

	public EditorNode DeserializeNode(string p_data) {
		var localData = p_data;
		return DeserializeNode(ref localData);
	}

	public EditorNode DeserializeNode(ref string p_data) {
		EditorNode _root = null;

			// If reached the end of the string data
        if(p_data.IndexOf(",", 1) == -1){
            return _root;
        }

			// Calculate Value
        var _val = p_data.Substring(",".Length, p_data.Substring(",".Length).IndexOf(","));

		// If no value present
        if(string.IsNullOrEmpty(_val)){
            return _root;                
        }
			
		string _id = Regex.Match(_val, @"-?\d+").Value;
        string _nodeType = _val.Replace(_id, "");

        // Evaluate Root
        if(_nodeType == "Dialogue") {
			_root = CreateDialogueNode("Dialogue Node", Vector2.zero);
		}
        else {
            Debug.Log("S6Error: Cannot deserialize symbol: " + _val);
        }

        _root.m_ID = int.Parse(_id);

        // Evaluate Left
        if(_root != null) {
			p_data = p_data.Substring(p_data.IndexOf(",", ",".Length));
			_root.m_LeftNode = DeserializeNode(ref p_data);
			if(_root.m_LeftNode != null) {
				OnClickOutPoint(_root.m_OutPoint);
				OnClickInPoint(_root.m_LeftNode.m_InPoint);
			}

			// Evaluate Right
			p_data = p_data.Substring(p_data.IndexOf(",", ",".Length));
			_root.m_RightNode = DeserializeNode(ref p_data);
			if(_root.m_RightNode != null) {
				OnClickOutPoint(_root.m_OutPoint);
				OnClickInPoint(_root.m_RightNode.m_InPoint);
			}
		}

        return _root;
	}

	private void LoadEditorData() {
		if (m_nodes != null) {
			m_nodes.Clear();
			m_connections.Clear();
		}

        if (m_serializeDataFileName == "" || m_serializeDataFileName == null) {
            return;
        }

        string _file = System.IO.Path.Combine(GetSaveDirectory(), m_serializeDataFileName + ".txt");
        if (_file == null || _file == "") {
            return;
        }

        StreamReader _streamReader = new StreamReader(_file);
		string _json = _streamReader.ReadToEnd();
		_streamReader.Close();

		SerializedJsonPack _serializedNodeData = JsonUtility.FromJson<SerializedJsonPack>(_json);

		var _test = DeserializeNode(_serializedNodeData.m_TreeData);
			
		for(int i = 0; i < _serializedNodeData.m_DialogueData.Length; ++i) {
            SerializedDialogueData _itrData = _serializedNodeData.m_DialogueData[i];
            EditorDialogueNode _match = SearchNode(_itrData.m_ID) as EditorDialogueNode;

            if (_match == null) {
                Debug.Log("S6Error: No matching node found for ID: " + _itrData.m_ID);
                return;
            }

            _match.m_Rect.position = _itrData.m_GraphPosition;
			_match.m_Speaker 		= _itrData.m_Writer;
			_match.m_DialogueText 	= _itrData.m_Message;
        }
	}
		
    private EditorNode SearchNode(int _nodeID) {
        for (int i = 0; i < m_nodes.Count; ++i) {
            if (m_nodes[i].m_ID == _nodeID) {
                return m_nodes[i];
            }
        }
		return null;
    }
    #endregion
}
#endif