﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

#if UNITY_EDITOR
public enum ConnectionPointType { In, Out }

[System.Serializable]
public sealed class EditorConnectionPoint {
	public Rect					m_Rect;
	public ConnectionPointType	m_Type;
	public EditorNode			m_Node;
	public GUIStyle				m_Style;

	public Action<EditorConnectionPoint> OnClickCallback;

	public EditorConnectionPoint(EditorNode p_node, ConnectionPointType p_type, GUIStyle p_style, Action<EditorConnectionPoint> p_onClickCallback) {
		m_Node = p_node;
		m_Type = p_type;
		m_Style = p_style;
		OnClickCallback = p_onClickCallback;
		m_Rect = new Rect(0, 0, 10f, 20f);
	}

	public void Draw() {
		m_Rect.y = m_Node.m_Rect.y + (m_Node.m_Rect.height * 0.5f) - m_Rect.height * 0.5f;

		switch (m_Type) {
			case ConnectionPointType.In:
				m_Rect.x = m_Node.m_Rect.x - m_Rect.width + 8f;
				break;
			case ConnectionPointType.Out:
				m_Rect.x = m_Node.m_Rect.x + m_Node.m_Rect.width - 8f;
				break;
		}

		if (GUI.Button(m_Rect, "", m_Style)) {
			if (OnClickCallback != null) {
				OnClickCallback(this);
			}
		}
	}
}
#endif