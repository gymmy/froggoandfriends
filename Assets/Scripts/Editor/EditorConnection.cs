﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

#if UNITY_EDITOR
using UnityEditor;

[System.Serializable]
public sealed class EditorConnection {
	public EditorConnectionPoint m_InPoint;
	public EditorConnectionPoint m_OutPoint;
	public Action<EditorConnection>	OnClickRemoveConnection;

	public EditorConnection(EditorConnectionPoint p_inPoint, EditorConnectionPoint p_outPoint, Action<EditorConnection> p_onRemoveConnection) {
		m_InPoint = p_inPoint;
		m_OutPoint = p_outPoint;
		OnClickRemoveConnection = p_onRemoveConnection;
	}

	public void Draw() {
		Handles.color = Color.yellow;
		Handles.DrawLine(m_InPoint.m_Rect.center, m_OutPoint.m_Rect.center);

		if(Handles.Button((m_InPoint.m_Rect.center + m_OutPoint.m_Rect.center) * 0.5f, Quaternion.identity, 4, 8, Handles.RectangleCap)) {
			if(OnClickRemoveConnection!=null) {
				OnClickRemoveConnection(this);
			}
		}
	}
}
#endif