﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

#if UNITY_EDITOR
using UnityEditor;

public class EditorDialogueNode : EditorNode {
    public string m_Speaker;
    public string m_DialogueText;

    public EditorDialogueNode(Vector2 p_position, float p_width, float p_height, GUIStyle p_inPointStyle, GUIStyle p_outPointStyle,
		Action<EditorConnectionPoint> p_OnClickInPoint, Action<EditorConnectionPoint> p_OnClickOutPoint,
		Action<EditorNode> p_onClickRemoveNode, int p_id) : base(p_position, p_width, p_height, p_inPointStyle, p_outPointStyle,
		p_OnClickInPoint, p_OnClickOutPoint, p_onClickRemoveNode, p_id) {
                                    
    }

	public override void Draw() {
		base.Draw();

        EditorGUI.LabelField(new Rect(m_Rect.x - 65, m_Rect.y + 40, m_Rect.width * 0.75f, 17), !String.IsNullOrWhiteSpace(m_Speaker) ? "": "Speaker");
        m_Speaker =  EditorGUI.TextField(new Rect(m_Rect.x -10, m_Rect.y + 40, 230, 17), m_Speaker);
        EditorGUI.LabelField(new Rect(m_Rect.x  - 65, m_Rect.y + 60, m_Rect.width * 0.75f, 17), !String.IsNullOrWhiteSpace(m_DialogueText) ? "": "Dialogue");
        m_DialogueText = EditorGUI.TextArea(new Rect(m_Rect.x - 10 , m_Rect.y + 60, 230, 30), m_DialogueText);

        m_WidthOffset = 100;
        m_HeightOffset = 250;
	}
}
#endif