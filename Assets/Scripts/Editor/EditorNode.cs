﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using System;
using UnityEditor;

[System.Serializable]
public abstract class EditorNode {
	[SerializeField]
	public Rect						m_Rect;
	[SerializeField]
	public string					m_Title = "Node";
	public int						m_ID;

	public bool						b_IsDragged;
	public bool						b_IsSelected;

	[SerializeField]
	public EditorConnectionPoint	m_InPoint;
	[SerializeField]
	public EditorConnectionPoint	m_OutPoint;

	public GUIStyle					m_Style;
	public GUIStyle					m_DefaultNodeStyle;
	public GUIStyle					m_SelectedNodeStyle;

	public EditorNode m_Parent;
	public EditorNode m_LeftNode;
	public EditorNode m_RightNode;

	public Action<EditorNode>		OnRemoveNode;

    public float m_WidthOffset = 0;
    public float m_HeightOffset = 0;

    Vector2 dragPosition;

	public EditorNode(Vector2 p_position, float p_width, float p_height, GUIStyle p_inPointStyle, GUIStyle p_outPointStyle, 
							Action<EditorConnectionPoint> p_OnClickInPoint, Action<EditorConnectionPoint> p_OnClickOutPoint,
							Action<EditorNode> p_onClickRemoveNode, int p_id) {

		SetupGUIStyle();
        //ID = GetHashCode();
        m_ID = p_id;

        m_Rect = new Rect(p_position.x, p_position.y, p_width, p_height);

		m_InPoint = new EditorConnectionPoint(this, ConnectionPointType.In, p_inPointStyle, p_OnClickInPoint);
		m_OutPoint = new EditorConnectionPoint(this, ConnectionPointType.Out, p_inPointStyle, p_OnClickOutPoint);

		OnRemoveNode = p_onClickRemoveNode;
	}

	protected virtual void SetupGUIStyle() {
		m_Style = new GUIStyle();
		m_Style.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/node1.png") as Texture2D;
		m_Style.border = new RectOffset(12, 12, 20, 20);
        m_Style.alignment = TextAnchor.MiddleCenter;
        m_Style.normal.textColor = Color.black;

        m_DefaultNodeStyle = m_Style;

		m_SelectedNodeStyle = new GUIStyle();
		m_SelectedNodeStyle.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/node1 on.png") as Texture2D;
		m_SelectedNodeStyle.border = new RectOffset(12, 12, 20, 20);
        m_SelectedNodeStyle.alignment = TextAnchor.MiddleCenter;
        m_SelectedNodeStyle.normal.textColor = Color.yellow;
    }
		
	public bool AddChild(EditorNode p_nextNode) {
		if(p_nextNode == null || p_nextNode.m_Parent != null) {
			return false;
		}

		if (p_nextNode == m_LeftNode || p_nextNode == m_RightNode) {
			p_nextNode.m_Parent = this;
			return true;
		}

		if(m_LeftNode == null) {
			m_LeftNode = p_nextNode;
			m_LeftNode.m_Parent = this;
			return true;
		}
		else if(m_RightNode == null) {
			m_RightNode = p_nextNode;
			m_RightNode.m_Parent = this;
			return true;
		}

		return false;
	}

	public void Drag(Vector2 p_delta) {
		m_Rect.position = p_delta;
	}

	public virtual void Draw() {
		m_InPoint.Draw();
		m_OutPoint.Draw();

        GUI.Box(m_Rect, "Dialogue", m_Style);

        if(b_IsDragged){
		GUIStyle _id = new GUIStyle();
		_id.alignment = TextAnchor.UpperLeft;
		_id.normal.textColor = Color.black;
        _id.fontSize = 9;
        GUI.Label(m_Rect, "ID:"+m_ID.ToString(), _id);

		GUIStyle _parent = new GUIStyle();
		_parent.alignment = TextAnchor.UpperRight;
		_parent.contentOffset = new Vector2(0, -20);
		_parent.normal.textColor = Color.black;
        _parent.fontSize = 9;
        string parentCond = "PID:" + ((m_Parent != null) ? m_Parent.m_ID.ToString() : "-");
        GUI.Label(m_Rect, parentCond, _parent);

		GUIStyle _left = new GUIStyle();
		_left.alignment = TextAnchor.UpperRight;
		_left.contentOffset = new Vector2(0, -10);
		_left.normal.textColor = Color.black;
		_left.fontSize = 9;
        string leftCond = "LID:" + ((m_LeftNode != null) ? m_LeftNode.m_ID.ToString() : "-");
        GUI.Label(m_Rect, leftCond, _left);

		GUIStyle _right = new GUIStyle();
		_right.alignment = TextAnchor.UpperRight;
        _right.contentOffset = new Vector2(0, 0);
        _right.normal.textColor = Color.black;
		_right.fontSize = 9;
		string rightCond = "RID:" + ((m_RightNode != null) ?  m_RightNode.m_ID.ToString() : "-");
		GUI.Label(m_Rect, rightCond, _right);
        }
	}

	public bool ProcessEvents(Event p_event, Vector2 p_scrollPosition) {
        Vector2 mousePos = p_event.mousePosition + p_scrollPosition;
		switch (p_event.type) {
			case EventType.MouseDown:
				if (p_event.button == 0) {
					if (m_Rect.Contains(mousePos)) {
						b_IsDragged = true;
						GUI.changed = true;
						b_IsSelected = true;
						m_Style = m_SelectedNodeStyle;
                        dragPosition = mousePos - m_Rect.position;
					} else {
						GUI.changed = true;
						b_IsSelected = false;
						m_Style = m_DefaultNodeStyle;
					}	
				}

				if (p_event.button == 1 && b_IsSelected && m_Rect.Contains(mousePos)) {
					ProcessContextMenu();
					p_event.Use();
				}
				break;

			case EventType.MouseUp:
				b_IsDragged = false;
				break;

			case EventType.MouseDrag:
				if (p_event.button == 0 && b_IsDragged) {
					Drag(mousePos - dragPosition);
					p_event.Use();
					return true;
				}
				break;
		}

		return false;
	}

	private void ProcessContextMenu() {
		GenericMenu _genericMenu = new GenericMenu();
		_genericMenu.AddItem(new GUIContent("Remove Node"), false, OnClickRemoveNode);
		_genericMenu.ShowAsContext();
	}

	public void RemoveChild(EditorNode node) {
		if(m_LeftNode == node) {
            m_LeftNode.m_Parent = null;
            m_LeftNode = null;

			// (NOTE) Reason is because left node is default read, so right node will be ignored as next node if we dont do this
            if (m_RightNode != null) {
                m_LeftNode = m_RightNode;
                m_RightNode = null;
            }
        }

		if(m_RightNode == node) {
            m_RightNode.m_Parent = null;
            m_RightNode = null;
		}
	}

	private void OnClickRemoveNode() {
		m_Parent?.RemoveChild(this);
		if(OnRemoveNode != null) {
			OnRemoveNode(this);
		}
	}
}
#endif