﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This simple AI uses a Hierarchal State Machine for movement. 
/// Definitely way overkill for a simple AI but serves as demonstration of how it works.
/// More suited for complex controls like action games.
/// </summary>

public enum Command
{
    Idle,
    Walk,
    Jump,
    Attack,
}

public abstract class State : UnityEngine.Object
{
    public virtual void OnEnter(RatEnemyActor p_enemy) { }
    public virtual void OnExit(RatEnemyActor p_enemy) { }
    public virtual void Update(RatEnemyActor p_enemy) { }
    public abstract void HandleCommand(RatEnemyActor p_enemy, Command p_command);
}

public class IdleState : State
{
    private float _timeUnderIdle;

    public override void OnEnter(RatEnemyActor p_enemy)
    {
        base.OnEnter(p_enemy);
    }

    public override void OnExit(RatEnemyActor p_enemy)
    {
        base.OnExit(p_enemy);
        _timeUnderIdle = 0.0f;
    }

    public override void Update(RatEnemyActor p_enemy)
    {
        _timeUnderIdle += Time.deltaTime;
        if (_timeUnderIdle > 2f)
        {
            HandleCommand(p_enemy, Command.Walk);
            _timeUnderIdle = 0.0f;
        }
    }

    public override void HandleCommand(RatEnemyActor p_enemy, Command p_command)
    {
        switch (p_command)
        {
            case Command.Walk:
                p_enemy.SetState<WalkState>();
                break;
            case Command.Attack:
                p_enemy.SetState<AttackState>();
                break;
            case Command.Jump:
                p_enemy.SetState<JumpState>();
                break;
            default:
                // Revert to idle state by default if none of the inheriting states can handle the command.
                p_enemy.SetState<IdleState>();
                break;
        }
    }
}

public class AttackState : IdleState
{
    public override void HandleCommand(RatEnemyActor p_enemy, Command p_command)
    {
        switch (p_command)
        {
            case Command.Walk:
                // Override base implementation and do nothing
                // Enemy cannot walk while attacking
                // Substitute to having multiple bools to check if enemy can walk while attacking
                break;
            case Command.Attack:
                // Override base implementation and do nothing
                // Enemy cannot attack while attacking
                break;
            case Command.Jump:
                // Override base implementation and do nothing
                // Enemy cannot jump while attacking
                break;
            default:
                base.HandleCommand(p_enemy, p_command);
                break;
        }
    }
}

public class WalkState : IdleState
{
    private float _timeUnderWalk;
    private int m_randomDirection = 1;

    public override void OnEnter(RatEnemyActor p_enemy)
    {
        base.OnEnter(p_enemy);
        m_randomDirection = Random.Range(0, 100) > 50 ? 1 : -1;
        
    }

    public override void Update(RatEnemyActor p_enemy)
    {
        _timeUnderWalk += Time.deltaTime;
        if (_timeUnderWalk > 1.0f)
        {
            HandleCommand(p_enemy, Command.Idle);
            _timeUnderWalk = 0f;
        }

        p_enemy.MoveHorizontal(1f * m_randomDirection);
    }

    public override void HandleCommand(RatEnemyActor p_enemy, Command p_command)
    {
        switch (p_command)
        {
            case Command.Walk:
                // Override base implementation and do nothing
                // Already walking
                break;
            case Command.Attack:
                // Override base implementation and do nothing
                // Enemy cannot walk if not idle
                break;
            case Command.Jump:
                // Walk state has no special behavior for jump, let parent handle it.
                // Let parent handle it
                base.HandleCommand(p_enemy, p_command);
                break;
            default:
                base.HandleCommand(p_enemy, p_command);
                break;
        }
    }
}

public class JumpState : IdleState
{
    public override void HandleCommand(RatEnemyActor p_enemy, Command p_command)
    {
        switch (p_command)
        {
            case Command.Walk:
                // Override base implementation and do nothing
                // Enemy cannot walk while jumping
                // Substitute to having multiple bools to check if enemy can walk while jumping
                break;
            case Command.Attack:
                // Override base implementation and do nothing
                // Enemy cannot attack while jumping
                break;
            case Command.Jump:
                // Override base implementation and do nothing
                // Enemy cannot jump while jumping
                break;
            default:
                base.HandleCommand(p_enemy, p_command);
                break;
        }
    }
}

public class RatEnemyActor : Actor, IUpdateable
{
    protected State m_currentState;

    public void Start()
    {
        SetState<IdleState>();
    }

    public void SetState<T>() where T : State , new()
    {
        T _state = new T();

        m_currentState?.OnExit(this);
        m_currentState = _state;
        m_currentState.OnEnter(this);
    }

    public void ManagedUpdate()
    {
        m_currentState?.Update(this);

        if (m_animator)
        {
            bool _isMoving = m_rigidbody.velocity.magnitude > 0;
            m_animator.SetBool("IsMoving", _isMoving);
        }

        Debug.DrawRay(transform.position + Vector3.left , Vector2.left * 1f, Color.red);
        Debug.DrawRay(transform.position + Vector3.right, Vector2.right * 1f, Color.red);
    }

    public override void OnCollisionEnter2D(Collision2D p_collision)
    {
        // Raycast sides whenever collision occurs to check if it should deal damage

        // check right
        RaycastHit2D _rightHit = Physics2D.Raycast(transform.position + Vector3.right, Vector2.right, 1f, (1 << LayerMask.NameToLayer("Actor")));
        if (_rightHit)
        {
            IDamageable _damageable = _rightHit.collider.gameObject.GetComponent<IDamageable>();
            if (_damageable != null)
            {
                if (_damageable.CanTakeDamage(this))
                {
                    Debug.Log("RDamage: " + _rightHit.collider.gameObject);
                    _damageable.TakeDamage(m_damage, this);
                }
            }
        }

        // check left
        RaycastHit2D _leftHit = Physics2D.Raycast(transform.position + Vector3.left, Vector2.left, 1f, (1 << LayerMask.NameToLayer("Actor")));
        if (_leftHit)
        {
            IDamageable _damageable = _leftHit.collider.gameObject.GetComponent<IDamageable>();
            if (_damageable != null)
            {
                if (_damageable.CanTakeDamage(this))
                {
                    Debug.Log("LDamage: " + _leftHit.collider.gameObject);
                    _damageable.TakeDamage(m_damage, this);
                }
            }
        }
    }
}
