﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EagleEnemyActor : Actor, IUpdateable
{
    [SerializeField] private float m_frequency = 20.0f;  // Speed of sine movement
    [SerializeField] private float m_magnitude = 0.5f;   // Size of sine movement
    [SerializeField] private Vector3 m_axis;
    
    public void ManagedUpdate()
    {
        Vector3 _pos = transform.position;
        _pos += transform.up * Time.deltaTime * m_baseMovementSpeed;
        m_rigidbody.MovePosition(_pos + m_axis * Mathf.Sin(Time.time * m_frequency) * m_magnitude);

        if (m_animator)
        {
            bool _isMoving = m_rigidbody.velocity.magnitude > 0;
            m_animator.SetBool("IsMoving", _isMoving);
        }

        Debug.DrawRay(transform.position + Vector3.left, Vector2.left * 1f, Color.red);
        Debug.DrawRay(transform.position + Vector3.right, Vector2.right * 1f, Color.red);
    }

    public override void OnCollisionEnter2D(Collision2D p_collision)
    {
        // Raycast sides whenever collision occurs to check if it should deal damage

        // check right
        RaycastHit2D _rightHit = Physics2D.Raycast(transform.position + Vector3.right, Vector2.right, 1f, (1 << LayerMask.NameToLayer("Actor")));
        if (_rightHit)
        {
            IDamageable _damageable = _rightHit.collider.gameObject.GetComponent<IDamageable>();
            if (_damageable != null)
            {
                if (_damageable.CanTakeDamage(this))
                {
                    Debug.Log("RDamage: " + _rightHit.collider.gameObject);
                    _damageable.TakeDamage(m_damage, this);
                }
            }
        }

        // check left
        RaycastHit2D _leftHit = Physics2D.Raycast(transform.position + Vector3.left, Vector2.left, 1f, (1 << LayerMask.NameToLayer("Actor")));
        if (_leftHit)
        {
            IDamageable _damageable = _leftHit.collider.gameObject.GetComponent<IDamageable>();
            if (_damageable != null)
            {
                if (_damageable.CanTakeDamage(this))
                {
                    Debug.Log("LDamage: " + _leftHit.collider.gameObject);
                    _damageable.TakeDamage(m_damage, this);
                }
            }
        }
    }
}
