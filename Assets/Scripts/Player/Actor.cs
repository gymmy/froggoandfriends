﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Responsibility: Handle entity.
/// </summary>
public class Actor : MonoBehaviour, IDamageable
{
    public enum Team
    {
        Player,
        Enemy,
    }

    [SerializeField] protected int m_maxHealth = 100;
    [SerializeField] protected int m_damage = 10;
    [SerializeField] protected float m_baseMovementSpeed = 1.0f;
    [SerializeField] protected float m_baseJumpForce = 300f;

    protected SpriteRenderer m_spriteRenderer;
    protected Rigidbody2D m_rigidbody;
    protected Collider2D m_collider;
    protected Animator m_animator;

    protected int m_currentHealth;
    protected bool b_isGrounded = true;
    public int m_SpriteOrientation { get; private set; }

    public Team m_Team { get; private set; }

    protected void ResetHealth()
    {
        m_currentHealth = m_maxHealth;
    }

    protected void SetupComponentReferences()
    {
        if (m_rigidbody == null)
        {
            m_rigidbody = GetComponent<Rigidbody2D>();
        }

        if (m_collider == null)
        {
            m_collider = GetComponent<Collider2D>();
        }

        if (m_animator == null)
        {
            m_animator = GetComponent<Animator>();
        }

        if (m_spriteRenderer == null)
        {
            m_spriteRenderer = GetComponent<SpriteRenderer>();
        }
    }

    protected virtual void Initialize()
    {
        ResetHealth();
    }

    public void OnEnable()
    {
        SetupComponentReferences();
        Initialize();
    }

    public virtual void MoveVertical(float p_direction)
    {

    }

    public virtual void MoveHorizontal(float p_direction)
    {
        if (b_isGrounded)
        {
            m_rigidbody.velocity = new Vector3(p_direction * m_baseMovementSpeed * Time.deltaTime, m_rigidbody.velocity.y, 0);
            UpdateSpriteOrientation(p_direction);
        }
    }

    public void UpdateSpriteOrientation(float p_direction)
    {
        if (m_spriteRenderer == null)
        {
            return;
        }

        if (p_direction > 0)
        {
            m_SpriteOrientation = 1;
            m_spriteRenderer.flipX = true;
        }
        else if (p_direction < 0)
        {
            m_SpriteOrientation = -1;
            m_spriteRenderer.flipX = false;
        }
    }

    public virtual bool CanJump()
    {
        return b_isGrounded;
    }

    public virtual void FireDown() { }

    public virtual void FireUp() { }

    public void Update()
    {
        Debug.DrawRay(transform.position, -Vector2.up, Color.red);
    }

    public virtual void OnCollisionEnter2D(Collision2D p_collision)
    {   
        // Check for ground
        if (!b_isGrounded)
        {
            // Check for gameobjects below the actor that isn't an actor
            RaycastHit2D _hit = Physics2D.Raycast(transform.position, Vector2.down, 5, ~(1 << LayerMask.NameToLayer("Actor")) );
            if (_hit)
            {
                b_isGrounded = true;
                m_rigidbody.velocity = Vector2.zero;
                m_animator.SetBool("IsGrounded", b_isGrounded);
            }
        }
    }

    public bool CanTakeDamage(Actor p_damageGiver)
    {
        // deadzone
        if (p_damageGiver == null)
        {
            return true;
        }

        return p_damageGiver != this && m_Team == p_damageGiver.m_Team;
    }

    public void TakeDamage(int p_damage, Actor p_damageGiver)
    {
        m_currentHealth -= p_damage;
        m_currentHealth = Mathf.Clamp(m_currentHealth, 0, m_currentHealth);

        if (m_currentHealth == 0)
        {
            DeathEvent _deathEvent = new DeathEvent(this, p_damageGiver);
            GameEventManager.BroadcastEvent<DeathEvent>(_deathEvent);

            Die();
        }
    }

    public virtual void Die()
    {
        gameObject.SetActive(false);
    }
}