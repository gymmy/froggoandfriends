﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDamageable
{
    void TakeDamage(int p_damage, Actor p_damageGiver);
    bool CanTakeDamage(Actor p_damageGiver);
}