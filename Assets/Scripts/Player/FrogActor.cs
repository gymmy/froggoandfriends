﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//           .-.   .-.
//          (o ) _(o )
//      __ / '-'   '-' \ __
//     /  /      "      \  \
//    |   \    _____,   /   |
//     \  \`-._______.-'/  /
// _.-`   /\)         (/\   `-._
//(_     / /  /.___.\  \ \     _)
// (_.(_/ /  (_ _)  \ \_)._)
//       (_(_) _)   (_(_) _)
public class FrogActor : Actor
{
    [Header("Movement-Jump")]
    [SerializeField] private float m_heldJumpTimeModifier = 2.0f;
    [SerializeField] private float m_heldJumpForceMin = 5.0f;
    [SerializeField] private float m_heldJumpForceMax = 50.0f;
    [Header("Movement-Horizontal")]
    [SerializeField] private float m_heldMoveTimeModifier = 2.0f;
    [SerializeField] private float m_heldMoveForceMin = 5.0f;
    [SerializeField] private float m_heldMoveForceMax = 50.0f;

    private int m_jumpDirection;
    private System.DateTime m_fireDownDate;

    protected override void Initialize()
    {
        base.Initialize();
        b_isGrounded = true;
        m_animator.SetBool("IsGrounded", b_isGrounded);
    }

    public override bool CanJump()
    {
        return b_isGrounded && m_jumpDirection != 0;
    }

    public override void FireDown()
    {
        m_fireDownDate = System.DateTime.Now;
    }

    public override void FireUp()
    {
        float _fireHeldTime = (float)(System.DateTime.Now - m_fireDownDate).TotalSeconds;
        float _modifiedJumpForce = (m_baseJumpForce * (_fireHeldTime * m_heldJumpTimeModifier)) * Time.deltaTime;
        _modifiedJumpForce = Mathf.Clamp(_modifiedJumpForce, m_heldJumpForceMin, m_heldJumpForceMax);

        float _modifiedMovementSpeed = (m_baseMovementSpeed * (_fireHeldTime * m_heldMoveTimeModifier)) * Time.deltaTime;
        _modifiedMovementSpeed = Mathf.Clamp(_modifiedMovementSpeed, m_heldMoveForceMin, m_heldMoveForceMax);

        if (CanJump())
        {
            m_rigidbody.AddForce(new Vector3(m_jumpDirection * _modifiedMovementSpeed, _modifiedJumpForce, 0));
            b_isGrounded = false;
            m_animator.SetBool("IsGrounded", b_isGrounded);
        }
    }

    public override void MoveHorizontal(float p_Direction)
    {
        if (b_isGrounded)
        {
            m_jumpDirection = Mathf.RoundToInt(p_Direction);
            UpdateSpriteOrientation(p_Direction);
        }  
    }

    public void OnCollisionEnter2D(Collision2D p_collision)
    {
        // Check for ground
        if (!b_isGrounded)
        {
            // Check for gameobjects below the actor that isn't an actor
            RaycastHit2D _hit = Physics2D.Raycast(transform.position, Vector2.down, 5, ~(1 << LayerMask.NameToLayer("Actor")));
            if (_hit)
            {
                b_isGrounded = true;
                m_rigidbody.velocity = Vector2.zero;
                m_animator.SetBool("IsGrounded", b_isGrounded);
            }

            if (p_collision.transform.position.y < transform.position.y)
            {
                IDamageable _damageable = p_collision.gameObject.GetComponent<IDamageable>();
                if (_damageable != null)
                {
                    if (_damageable.CanTakeDamage(this))
                    {
                        _damageable.TakeDamage(m_damage, this);
                    }
                }
            }
        }
    }
}