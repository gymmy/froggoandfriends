﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Listens to SwapActorEvent from PlayerController to swap active player actor.
/// </summary>
public class PlayerActorSwapper : MonoBehaviour
{
    [SerializeField] private List<Actor> m_actors = new List<Actor>();
    private Actor m_currentActor;

    private void OnEnable()
    {
        GameEventManager.AddListener<SwapActorEvent>(SwapActor);
    }

    private void SwapActor(GameEvent p_event)
    {
        SwapActorEvent _castEvent = (SwapActorEvent) p_event;
       
        int _swapActorIndex = _castEvent.m_SwapActorIndex;

        if (CanSwapToActor(_swapActorIndex))
        {
            Actor _actorToSwap = m_actors[_swapActorIndex];

            // if there is existing current actor, cleanup first
            if (m_currentActor != null)
            {
                _actorToSwap.transform.localPosition = m_currentActor.transform.localPosition;
                _actorToSwap.UpdateSpriteOrientation(m_currentActor.m_SpriteOrientation);
                m_currentActor.gameObject.SetActive(false);
            }

            m_currentActor = _actorToSwap;
            _actorToSwap.gameObject.SetActive(true);
            PlayerController.Get().Possess(_actorToSwap);
        }
    }

    private bool CanSwapToActor(int p_actorIndex)
    {
        return m_actors.Count > p_actorIndex && m_actors[p_actorIndex] != null;
    }
}
