﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EagleActor : Actor
{
    public override bool CanJump()
    {
        return false;
    }

    public override void MoveVertical(float p_direction)
    {
        m_rigidbody.velocity = new Vector3(m_rigidbody.velocity.x, p_direction * m_baseMovementSpeed * Time.deltaTime, 0);
    }
}
