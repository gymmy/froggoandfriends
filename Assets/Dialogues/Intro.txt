{
    "m_TreeData": ",1241382912Dialogue,1319436288Dialogue,1338458112Dialogue,1336606720Dialogue,1268047872Dialogue,1221427200Dialogue,1216495616Dialogue,1251663872Dialogue,1248239616Dialogue,1219522560Dialogue,1261002752Dialogue,1235079168Dialogue,1329614848Dialogue,,,,,1246470875Dialogue,1260195840Dialogue,,,,,,,,,,1234796544Dialogue,1238425600Dialogue,,,,,",
    "m_DialogueData": [
        {
            "m_ID": 1241382912,
            "m_GraphPosition": {
                "x": 16.0,
                "y": 61.0,
                "z": 0.0
            },
            "m_Writer": "Froggo",
            "m_Message": "Hello!"
        },
        {
            "m_ID": 1319436288,
            "m_GraphPosition": {
                "x": 267.0,
                "y": 63.0,
                "z": 0.0
            },
            "m_Writer": "Froggo",
            "m_Message": "I'm Froggo, welcome to my game!"
        },
        {
            "m_ID": 1338458112,
            "m_GraphPosition": {
                "x": 546.0,
                "y": 71.0,
                "z": 0.0
            },
            "m_Writer": "Froggo",
            "m_Message": "Would you like a quick review of the controls? "
        },
        {
            "m_ID": 1336606720,
            "m_GraphPosition": {
                "x": 836.0,
                "y": 32.0,
                "z": 0.0
            },
            "m_Writer": "",
            "m_Message": "I love instructions!"
        },
        {
            "m_ID": 1268047872,
            "m_GraphPosition": {
                "x": 1126.0,
                "y": 31.0,
                "z": 0.0
            },
            "m_Writer": "Froggo",
            "m_Message": "Oh boy! A great listener!"
        },
        {
            "m_ID": 1221427200,
            "m_GraphPosition": {
                "x": 1376.0,
                "y": 31.0,
                "z": 0.0
            },
            "m_Writer": "Froggo",
            "m_Message": "Hold a horizontal direction[A,D,Left,Right] towards where you want me to hop..."
        },
        {
            "m_ID": 1216495616,
            "m_GraphPosition": {
                "x": 1623.0,
                "y": 34.0,
                "z": 0.0
            },
            "m_Writer": "Froggo",
            "m_Message": "...then hold [Space/Mouse0] for how high you want me to jump..."
        },
        {
            "m_ID": 1251663872,
            "m_GraphPosition": {
                "x": 1891.0,
                "y": 36.0,
                "z": 0.0
            },
            "m_Writer": "Froggo",
            "m_Message": "...and then let go of the button to set me loose!"
        },
        {
            "m_ID": 1248239616,
            "m_GraphPosition": {
                "x": 2157.0,
                "y": 40.0,
                "z": 0.0
            },
            "m_Writer": "Froggo",
            "m_Message": "Pretty cool right? I would totally give my programmer an A+ if I had to grade him!"
        },
        {
            "m_ID": 1219522560,
            "m_GraphPosition": {
                "x": 2432.0,
                "y": 38.0,
                "z": 0.0
            },
            "m_Writer": "Froggo",
            "m_Message": "Do you have any questions? "
        },
        {
            "m_ID": 1261002752,
            "m_GraphPosition": {
                "x": 2727.0,
                "y": 41.0,
                "z": 0.0
            },
            "m_Writer": "",
            "m_Message": "How do I verify multiple characters on my exam checklist?"
        },
        {
            "m_ID": 1235079168,
            "m_GraphPosition": {
                "x": 3003.0,
                "y": 46.0,
                "z": 0.0
            },
            "m_Writer": "Froggo",
            "m_Message": "Why, you simply have to press 1 or 2 to alternate between me and Eagleboy!"
        },
        {
            "m_ID": 1329614848,
            "m_GraphPosition": {
                "x": 3275.0,
                "y": 41.0,
                "z": 0.0
            },
            "m_Writer": "Froggo",
            "m_Message": "Alright them, that'll be all. Thanks for listening to my dialogues. Have fun!"
        },
        {
            "m_ID": 1246470875,
            "m_GraphPosition": {
                "x": 2238.0,
                "y": 227.0,
                "z": 0.0
            },
            "m_Writer": "",
            "m_Message": "None, I already know I just need to press 1 and 2 to swap characters."
        },
        {
            "m_ID": 1260195840,
            "m_GraphPosition": {
                "x": 2549.0,
                "y": 227.0,
                "z": 0.0
            },
            "m_Writer": "Froggo",
            "m_Message": "Alright then, that'll be all. Have fun!"
        },
        {
            "m_ID": 1234796544,
            "m_GraphPosition": {
                "x": 819.0,
                "y": 152.0,
                "z": 0.0
            },
            "m_Writer": "",
            "m_Message": "Real gamers don't need tutorials!!!"
        },
        {
            "m_ID": 1238425600,
            "m_GraphPosition": {
                "x": 1116.0,
                "y": 158.0,
                "z": 0.0
            },
            "m_Writer": "Froggo",
            "m_Message": "Okay! Just remember to press \"=\" to get restart the level if you get stuck."
        }
    ]
}